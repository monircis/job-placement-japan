
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html"/>
    <meta name="author" content="NepsterJay"/>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>
        Landing Page
    </title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="alarea">
<?php include 'create.php'; ?>
</div>
<div class="verticalhorizontal">
    <div class="musk">
        <div class="left">
            <div class="textwidget">
                <div class="thim-get-100s">
                    <p class="get-100s">
                        Get 100s of online
                        <span class="thim-color">
									Courses For Free
								</span>
                    </p>
                    <h2>
                        Ask Your <span class="thim-color">Question</span> here
                    </h2>
                </div>
            </div>
        </div>
        <div class="right">
            <div>
                <div class="forms">
                    <h3 class="title">
								<span>
									Please Insert your Question 
								</span>
                    </h3>
                    <div role="form" class="wpcf7" id="wpcf7-f85-p5900-o1" dir="ltr" lang="en-US">
                        <div class="screen-reader-response">
                            <p role="status" aria-live="polite" aria-atomic="true">
                            </p>
                            <ul>
                            </ul>
                        </div>
                        <form action="index.php" id="formId" method="post" class="wpcf7-form init">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input required autofocus type="text" name="name" value="" class="form-control f1"
                                               placeholder="Your Name *">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input required type="email" name="email" value="" class="form-control f2"
                                               placeholder="Email *">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" required name="phone" value="" class="form-control f3"
                                               placeholder="Phone *">

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="finalEducation" value="" class="form-control f4" required
                                               placeholder="Education Qualification *">

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control f5" name="jobStatus" required>
                                            <option value="">Select Job Status</option>
                                            <option value="Student">Student</option>
                                            <option value="Job Holder">Job Holder</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 fieldone">
                                    <div class="form-group">
                                        <input type="text" name="studyinAt" value="" class="form-control f6"
                                               placeholder="Current Education *">

                                    </div>
                                </div>
                                <div class="col-md-12 fieldone">
                                    <div class="form-group">
                                        <input type="text" name="instituteName" value="" class="form-control f7"
                                               placeholder="Institute Name *">

                                    </div>
                                </div>
                                <div class="col-md-12 fieldone">
                                    <div class="form-group">
                                        <input type="text" name="expeGradYear" value="" class="form-control f8"
                                               placeholder="Expected Graduation Year *">

                                    </div>
                                </div>

                                <div class="col-md-12 fieldTwo">
                                    <div class="form-group">
                                        <input type="text" name="designation" value="" class="form-control f9"
                                               placeholder="Designation *">

                                    </div>
                                </div>
                                <div class="col-md-12 fieldTwo">
                                    <div class="form-group">
                                        <input type="text" name="companyName" value="" class="form-control f10"
                                               placeholder="Company Name *">

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea placeholder="Please Insert Your Question" class="form-control f11" required
                                                  name="question"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit"   class="btn btn-orange btn-block">Submit</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
<script>


    $(".fieldTwo").hide();
    $(".fieldone").hide();

    $('select').on('change', function() {
        $(".fieldTwo input").prop('required',false);
        $(".fieldone input").prop('required',false);
        if(this.value=='Job Holder'){
            $(".fieldTwo").show();
            $(".fieldone").hide();
            $(".fieldTwo input").prop('required',true);
        }
        if(this.value=='Student'){
            $(".fieldTwo").hide();
            $(".fieldone").show();
            $(".fieldone input").prop('required',true);
        }
    });


</script>
</body>

</html>